/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author universidad
 */
@WebServlet(name = "BienvenidaServlet", urlPatterns = {"/procesar"})
public class BienvenidaServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String miNombre = request.getParameter("nombre");
        HashMap<String, Object> errores = new HashMap();
        Integer miEdad = Integer.valueOf(request.getParameter("edad"));
        
        if(miNombre == null || miNombre.length() == 0) {
            errores.put("nombre", "El nombre no puede ser vacío");
        }
        
        if(errores.isEmpty()) {
            //variable a disposicion del jsp
            request.setAttribute("nombre", miNombre);
            request.setAttribute("edad", miEdad);
            request.getRequestDispatcher("WEB-INF/jsp/datos_ok.jsp").forward(request, response);
            
        } else {
            //forward al form con los errores
            request.setAttribute("errores", errores);
            request.getRequestDispatcher("WEB_INF/jsp/form.jsp").forward(request, response);
        }     
    }
}
